How to Run this project:

1. To run backend service
    *  navigate to  /cloned-dir/backend
    *  nodemon server (this command will start backend server at port 5000)
    Finally backend service is running - [](http://localhost:5000)

2. To run frontend service
    *  navigate to /cloned-dir/
    *  npm start (this command will start frontend service at port 3000)
    Finally frontend service is running - [](http://localhost:3000)

Now app is running, we can access at [](http://localhost:3000)

Project Details: Booking system

    *  You can book available airport package service
    *  user can login and logout from application
    
    *  as registration functionality is not available, login using below credentials:
        
        username: admin@gmail.com
        password: #admin123

    *  while booking if user is not loggedin asking user details
    *  user can pay through any debit/credit card to book our service
        
        to test payment gateway service use card available in the below link
        [](https://stripe.com/docs/testing#cards)

    *  to get list of registrations done by users, make this api call in postman
        method: GET
        url: http://localhost:5000/api/payments
