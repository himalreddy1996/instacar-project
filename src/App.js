import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import "bootstrap/dist/css/bootstrap.min.css";

import Navbar from "./components/navbar.component";
import Home from "./components/home.component";
import OneWay from "./components/oneway.component";
import RoundTrip from "./components/roundtrip.component";
import Multicity from "./components/multicity.component";
import Package from "./components/package.component";
import Login from "./components/login.component";
import Logout from "./components/logout.component";
import Booking from "./components/booking.component";

function App() {
  return (
    <Router>
      <Navbar />
      <br/>
      <Route path="/" exact component={Home} />
      <Route path="/qa-index" exact component={Home} />
      <Route path="/qa-index/one-way" component={OneWay} />
      <Route path="/qa-index/round-trip" component={RoundTrip} />
      <Route path="/qa-index/multicity" component={Multicity} />
      <Route path="/qa-index/package" component={Package} />
      <Route path="/qa-index/login" component={Login} />
      <Route path="/qa-index/logout" component={Logout} />
      <Route path="/qa-index/booking/package" component={Booking} />
    </Router>
  );
}

export default App;
