import React, { Fragment } from "react";
import StripeCheckout from "react-stripe-checkout";
import axios from "axios";

const stripeBtn = (props) => {
  const publishableKey = "pk_test_WfUKymt4LgnwAIe0U4Ru8pVe00LVVHxvFh";
  console.log(props);

  const onToken = token => {
    const body = {
      amount: parseFloat(props.data.packagePrice),
      token: token,
      data: props.data
  };

  axios.post("/api/payment", body)
      .then(response => {
        console.log(response);
        if(response.data == "success"){
          document.querySelector(".package_submit_div").classList.add("disabled");
          document.querySelector(".package_submit_div input").value = "Payment Success";
          setTimeout(function(){
            document.querySelector(".package_submit_div input").value = "redirecting to main page";
            setTimeout(function(){
              window.location="/qa-index/package";
            },2000);
          },2000);
        }else{
          console.log("pending");
        }
      })
      .catch(error => {
        console.log("Payment Error: ", error);
        alert("Payment Error");
      });
  };

  return (
    <StripeCheckout
      label="Booking System" //Component button text
      name="Booking System" //Modal Header
      description={props.data.packageName}
      panelLabel="Continue to Payment" //Submit button in modal
      amount={parseFloat(props.data.packagePrice)} //Amount in cents $9.99
      token={onToken}
      stripeKey={publishableKey}
      billingAddress={false}
    />
  );
};

export default stripeBtn;
