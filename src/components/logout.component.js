import React, { Component } from 'react';

export default class Logout extends Component {

  constructor(props){
    super(props);

    this.state = {
      loginStatus: true
    }
  }
  componentDidMount(){
    localStorage.removeItem("instacar_userdata");    
    window.location = "/qa-index/package";
  }

  render() {
    return <div className="container">Logged Out Successfully </div>;
  }
}
