import React, { Component } from 'react';
import axios from 'axios';
import "../stylesheets/booking.css";
//import { Link } from 'react-router-dom';
import StripeBtn from "./stripeBtn.js";
import DateTimePicker from 'react-datetime-picker';
//import DateTimePicker from 'react-datetime-picker/dist/entry.nostyle';

const UserDetailsForm = props => (
  <div className="UserDetails">
    <div className="form-group userdetails_name_div">
      <label>Name: </label>
      <input type="text" ref={props.event.inputNameRef}
          className="form-control name_input"
          value={props.user.name}
          onChange={props.event.onChangeName}
          placeholder="name"
          />
    </div>
    <div className="form-group userdetails_email_div">
      <label>E mail: </label>
      <input type="email" ref={props.event.inputEmailRef}
          className="form-control emailid_input"
          value={props.user.emailId}
          onChange={props.event.onChangeEmailId}
          placeholder="email id"
          />
    </div>
    <div className="form-group userdetails_email_div">
      <label>Phone: </label>
      <input type="number" ref={props.event.inputPhoneRef}
          className="form-control phoneno_input"
          value={props.user.phoneNo}
          onChange={props.event.onChangePhoneNo}
          placeholder="phone number"
          />
    </div>
  </div>
)


export default class Booking extends Component {

  constructor(props){
    super(props);

    this.getUserDetailsForm = this.getUserDetailsForm.bind(this);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeEmailId = this.onChangeEmailId.bind(this);
    this.onChangePhoneNo = this.onChangePhoneNo.bind(this);
    this.onChangeAddress = this.onChangeAddress.bind(this);
    this.onContinuePayment = this.onContinuePayment.bind(this);
    this.onChangePickupDate = this.onChangePickupDate.bind(this);

    this.inputNameRef = React.createRef();
    this.inputEmailRef = React.createRef();
    this.inputPhoneRef = React.createRef();
    this.inputAddressRef = React.createRef();

    this.state = {
      packageId : "",
      packageName: "",
      packagePrice: 0,
      address: "",
      pickupDate: new Date(),
      userData: {
        id: "",
        name: "",
        userName: "",
        phoneNo: "",
        emailId: ""
      }
    }
  }
  componentDidMount() {
    //console.log(this);
    let queryStrings = window.location.search.substring(1);
  	let params = queryStrings.split('&');
    let queryParams = [];
    let queryParamsCount =0;
  	for (var i = 0; i < params.length; i++) {
      queryParamsCount+=1;
  		var pair = params[i].split('=');
  		queryParams[pair[0]] = decodeURIComponent(pair[1]);
  	}
    //console.log(queryParams["id"]);
    //console.log(typeof queryParams["ssc"]);
  	if(queryParamsCount > 0 && queryParams["id"] !== undefined){
      this.setState({
        packageId: queryParams["id"]
      })
      const packageId = queryParams["id"];
      axios.get("/api/packages/"+packageId)
        .then(response => {
          if(response.statusText = "OK" && typeof response.data.name !== undefined){
            this.setState({
              packageId: response.data.id,
              packageName: response.data.name,
              packagePrice: response.data.price
            })
          }
        })
        .catch((error) => {
          console.log(error);
        })

        let userData = localStorage.getItem("instacar_userdata");
        if(typeof userData !== undefined && userData != null){
          this.setState({
            userData: JSON.parse(userData)
          })
        }
    }else{
      console.log("need to handle this case");
    }
  }
  getUserDetailsForm(){
      return <UserDetailsForm user={this.state.userData} event={this} />;
  }

  onChangeName(e){
    const userdata = this.state.userData;
    userdata.name = e.target.value;
    this.setState({
      userData: userdata
    })
    console.log(this.state);
  }

  onChangeEmailId(e){
    const userdata = this.state.userData;
    userdata.emailId = e.target.value;
    this.setState({
      userData: userdata
    })
  }
  onChangePhoneNo(e){
    const userdata = this.state.userData;
    userdata.phoneNo = e.target.value;
    this.setState({
      userData: userdata
    })
  }
  onChangeAddress(e){
    this.setState({
      address: e.target.value
    })
  }
  onChangePickupDate(e){
    console.log(e);
    this.setState({
      pickupDate: e
    })
  }
  onContinuePayment(){
      const { name, emailId, phoneNo } = this.state.userData;
      const { address } = this.state;
      if(name.length <= 3){
        alert("your name should be more than 3 chars");
        this.inputNameRef.current.focus();
        return;
      }else if(!(/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(emailId))){
        alert("enter valid email address");
        this.inputEmailRef.current.focus();
        return;
      }else if(phoneNo.length != 10){
        alert("enter valid 10 digits mobile number");
        this.inputPhoneRef.current.focus();
        return;
      }else if(address.length == 0){
        alert("enter address to pickup");
        this.inputAddressRef.current.focus();
        return;
      }
      document.querySelector("#paymentgatewaystrip .StripeCheckout").click();
  }

  render() {
    return (
      <div className="Booking container">
        <div className="innercontainer card">
          { this.getUserDetailsForm() }
          <div className="PackageDetails">
            <div className="form-group package_name_div">
              <label>Package: </label>
              <input type="text" readOnly
                  className="form-control package_name_input"
                  value={this.state.packageName}
                  />
            </div>
            <div className="package_price_div">
              <label>Price: </label>
              <input type="text" readOnly
                  className="form-control package_price_input"
                  value={this.state.packagePrice}
                  />
            </div>
          </div>
          <div className="DeliveryDetails">
            <div className="pickupdate">
              <label>Pickup Date&Time:  </label>
              <DateTimePicker format='dd-MM-yyyy HH:mm' onChange={this.onChangePickupDate} value={this.state.pickupDate} />
            </div>
            <div className="address">
              <label>Pickup Address: </label>
              <textarea ref={this.inputAddressRef} value={this.state.address} placeholder="address" onChange={this.onChangeAddress} />
            </div>
          </div>
          <div className="ContinueButton">
            <span className="form-group package_submit_div" onClick={this.onContinuePayment}>
              <input type="submit" value="Continue Payment" className="btn btn-primary" />
            </span>
            <div id="paymentgatewaystrip" className="paymentgatewaystrip">
              <StripeBtn data={this.state} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
