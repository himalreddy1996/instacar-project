import React, { Component } from 'react';
import axios from 'axios';
import "../stylesheets/login.css";

export default class Login extends Component {

  constructor(props){
    super(props);

    this.onChangeUserName = this.onChangeUserName.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.onLogin = this.onLogin.bind(this);

    this.state = {
      userName: "",
      phoneNo: "",
      emailId:"",
      password:""
    }
    this.inputUserNameRef = React.createRef()
    this.inputPasswordRef = React.createRef()
  }
  onChangeUserName(e){
    this.setState({
      userName: e.target.value
    })
  }
  onChangePassword(e){
    this.setState({
        password: e.target.value
    })
  }
  onLogin(e){
    //e.preventDefault();
    const { userName, phoneNo, emailId, password } = this.state;

    if(userName.length == 0){
      alert("please enter username");
      this.inputUserNameRef.current.focus();
      return;
    }else if(password.length == 0){
      alert("please enter password");
      this.inputPasswordRef.current.focus();
      return;
    }

    const userdetails = {
      userName,
      password
    };
    //+"?username="+this.state.userName+"&password="+this.state.password
    axios.post('http://localhost:3000/api/login', userdetails)
      .then(response => {
        if(response.data.status == "success"){
          //localStorage.setItem("instacar_userName", response.data.userDetails.userName);
          //localStorage.setItem("instacar_phoneNo", response.data.userDetails.phoneNo);
          //localStorage.setItem("instacar_emailId", response.data.userDetails.emailId);
          localStorage.setItem("instacar_userdata", JSON.stringify(response.data.userDetails));
          alert("Loged In successfully");
          window.location = "/qa-index/package";
        }
        else{
          alert(response.data.msg);
        }
      })
      .catch((error) => {
        console.log(error);
      })
  }

  render() {
    return (
      <div className="Login container">
        <div class="innercontainer card">
          <h4>Login</h4>
          <div className="form-group username_input_div">
            <label>Username: </label>
            <input type="text" ref={this.inputUserNameRef}
                required
                className="form-control username_input"
                value={this.state.userName}
                onChange={this.onChangeUserName}
                placeholder="user name"
                />
          </div>
          <div className="form-group password_input_div">
            <label>Password: </label>
            <input type="password" ref={this.inputPasswordRef}
                required
                className="form-control username_input"
                value={this.state.password}
                onChange={this.onChangePassword}
                placeholder="password"
                />
          </div>
          <div className="form-group login_submit_div" onClick={this.onLogin}>
            <input type="submit" value="Login" className="btn btn-primary" />
          </div>
        </div>
      </div>
    );
  }
}
