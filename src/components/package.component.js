import React, { Component } from 'react';
import axios from 'axios';
import "../stylesheets/package.css";
export default class Package extends Component {

  constructor(props){
    super(props);

    this.onChangePackage = this.onChangePackage.bind(this);
    this.onBookNow = this.onBookNow.bind(this);

    this.state = {
      packages: [],
      username: "guest",
      selectedPackage:"",
      price:0
    }
  }
  componentDidMount() {
    axios.get('/api/packages')
      .then(response => {
        console.log(response);
        console.log(typeof(response.data));
        if (response.data.length > 0) {
          this.setState({
            packages: response.data,
            selectedPackage: response.data[0].id,
            price: response.data[0].price
          })
        }
      })
      .catch((error) => {
        console.log(error);
      })
  }

  onChangePackage(e) {
    this.setState({
      selectedPackage: e.target.value,
      price: e.target.querySelector("option[value='"+e.target.value+"']").getAttribute("price")
    })
  }
  onBookNow(){
    window.location = "/qa-index/booking/package?id="+this.state.selectedPackage
  }

  render() {
    return (
      <div className="Package container">
        <div className="innercontainer card">
          <h4>Select Package</h4>
          <div className="form-group package_input_div">
            <label>Package: </label>
            <select
                className="form-control package_select_box"
                value={this.state.selectedPackage}
                onChange={this.onChangePackage}>
                {
                  this.state.packages.map(function(eachPackage) {
                    return <option value={eachPackage.id} key={eachPackage.id} price={eachPackage.price}>
                        {eachPackage.name}
                      </option>;
                  })
                }
            </select>
          </div>
          <div className="form-group price_input_div">
            <label>Price: </label>
            <input type="text" readOnly
                className="form-control price_input"
                value={this.state.price}
                />
          </div>
          <div className="form-group package_submit_div">
            <input type="submit" value="Book Now" className="btn btn-primary" onClick={this.onBookNow}/>
          </div>
        </div>
      </div>
    );
  }
}
